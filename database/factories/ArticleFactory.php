<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ArticleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->sentence(6, true);
        $slug = Str::slug($title);

        return [
            'title' => $title,
            'slug' => $slug,
            'body' => $this->faker->paragraph(100, true),
            'img' => 'https://via.placeholder.com/300/0000FF/FFFFFF?text=Laravel8',
            'created_at' => $this->faker->dateTimeBetween('-1 years')
        ];
    }
}
